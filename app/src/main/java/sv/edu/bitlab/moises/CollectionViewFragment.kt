package sv.edu.bitlab.moises

import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.text.SpannableString
import android.text.TextPaint
import android.text.style.UnderlineSpan
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.firebase.ui.firestore.FirestoreRecyclerOptions
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import sv.edu.bitlab.moises.AccountRecycler.AccountAdapter
import sv.edu.bitlab.moises.dataclass.Account
import sv.edu.bitlab.moises.R.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [ExitoFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [ExitoFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class CollectionViewFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var listener: OnFragmentInteractionListener? = null
    private var adapteR: AccountAdapter?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(layout.fragment_exito, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val bd = FirebaseFirestore.getInstance()
        val colecctionRef = bd.collection("accounts")
        var query = colecctionRef.orderBy("accountName", Query.Direction.DESCENDING)
        val options = FirestoreRecyclerOptions.Builder<Account>()
            .setQuery(query, Account::class.java)
            .build()

        adapteR = AccountAdapter(this.requireContext(),options)
        var recycler = view.findViewById<RecyclerView>(R.id.recycler_accounts)
        recycler!!.setHasFixedSize(true)
        recycler!!.layoutManager = LinearLayoutManager(this.context)
        recycler.adapter = adapteR

        //subrayado
        makeUnderlineText()

        view?.findViewById<TextView>(R.id.nuevo_registro).setOnClickListener {
            activity?.onBackPressed()
        }


    }


    private fun makeUnderlineText(){
        val nuevo_registro = view?.findViewById<TextView>(R.id.nuevo_registro)
        val content = SpannableString(getString(string.agregar_un_nuevo_registro))
        val underlineSpan = UnderlineSpan()
        val tp = TextPaint()
        tp.color = Color.WHITE
        underlineSpan.updateDrawState(tp)
        content.setSpan(underlineSpan, 0, content.length, 0)
        nuevo_registro?.text=content
    }

    override fun onStart() {
        super.onStart()
        adapteR?.startListening()
    }

    override fun onStop() {
        super.onStop()
        adapteR?.startListening()
    }

    fun onButtonPressed(uri: Uri) {
        listener?.onFragmentInteraction(uri)
    }

    /*override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }*/


    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {

        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            CollectionViewFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}
