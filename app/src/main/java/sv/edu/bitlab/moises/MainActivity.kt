package sv.edu.bitlab.moises

import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import android.widget.FrameLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.firebase.ui.firestore.FirestoreRecyclerOptions
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import sv.edu.bitlab.moises.AccountRecycler.AccountAdapter
import sv.edu.bitlab.moises.dataclass.Account

class MainActivity : AppCompatActivity(), BitlabFragment.OnFragmentInteractionListener{



    override fun montarFragmentRecycler(frag: CollectionViewFragment) {
        val fragmentManager = supportFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.fragment_container, frag).addToBackStack(null)
        fragmentTransaction.commit()

    }

    override fun succesMessage(idcontenedor: View) {
        val updateHandler = Handler(Looper.getMainLooper())
        //showLoading(true)
        idcontenedor.visibility = View.VISIBLE
        val runnable = Runnable{
          //  showLoading(false)
            idcontenedor.visibility = View.GONE
            //montarFragmentRecycler(CollectionViewFragment())
        }
        updateHandler.postDelayed(runnable, 9000)

    }

    override fun onBackPressed() {
        super.onBackPressed()
    }


    override fun onFragmentInteraction(uri: Uri) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        addFragment(BitlabFragment())
    }

    fun addFragment(frag: BitlabFragment){
        val fragmentManager = supportFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.fragment_container, frag)
        fragmentTransaction.commit()
    }

}
