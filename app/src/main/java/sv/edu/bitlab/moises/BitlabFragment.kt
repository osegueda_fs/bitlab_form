package sv.edu.bitlab.moises
import android.content.ContentValues.TAG
import android.content.Context
import android.graphics.Color

import android.net.Uri
import android.os.Bundle

import android.text.SpannableString
import android.text.TextPaint
import android.text.style.UnderlineSpan
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*

import com.google.firebase.firestore.FirebaseFirestore

import com.google.firebase.storage.FirebaseStorage
import kotlinx.android.synthetic.main.fragment_bitlab.*

import sv.edu.bitlab.moises.R.*
import sv.edu.bitlab.moises.dataclass.Account


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [BitlabFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [BitlabFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class BitlabFragment : Fragment(){
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var listener: OnFragmentInteractionListener? = null
    var urll: String?=null




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(layout.fragment_bitlab, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val button = view.findViewById<Button>(R.id.send_button)
        //custom spinner
        val spinnercolor = view.findViewById<Spinner>(R.id.id_spinner)
        ArrayAdapter.createFromResource(
            requireContext(),
            array.array_options,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            // Specify the layout to use when the list of choices appears
            adapter.setDropDownViewResource(layout.spinner_dropdown)
            // Apply the adapter to the spinner
            spinnercolor.adapter = adapter
        }



        //texto subrayado
        makeUnderlineText()

        //Envio de data y succes con handler

        button.setOnClickListener {
            val idContainer = view.findViewById<View>(R.id.loading_container)

            if (validarEnvio()){
                firebaseStorage()
                firebaseFirestore()
                listener?.succesMessage(idContainer)
                listener?.montarFragmentRecycler(CollectionViewFragment())
                //setRecyclerView()
            }

        }

        id_label_coleccion.setOnClickListener {
            listener?.montarFragmentRecycler(CollectionViewFragment())
        }



    }



    private fun makeUnderlineText(){
        val coleccion = view?.findViewById<TextView>(R.id.id_label_coleccion)
        val content = SpannableString(getString(string.collection))
        val underlineSpan = UnderlineSpan()
        val tp = TextPaint()
        tp.color = Color.WHITE
        underlineSpan.updateDrawState(tp)
        content.setSpan(underlineSpan, 0, content.length, 0)
        coleccion?.text=content
    }

    private fun firebaseStorage(){

      val storager = FirebaseStorage.getInstance().reference
                val rutaimagen = Uri.parse("android.resource://" + context?.packageName + "/drawable/usericon")
                val imageRef = storager.child("accounts-image/${System.currentTimeMillis()}_usericon.png")

                imageRef.putFile(rutaimagen).addOnSuccessListener {
                    Log.d("envio","se envio con exito")


                    imageRef.downloadUrl.addOnCompleteListener{ taskSnapshot ->

                        //taskSnapshot.result.toString()
                        //tomo la url de la imagen en firebase
                        urll = taskSnapshot.result.toString()
                        Log.i(TAG, "url de imagen: $urll")
                        //firebaseFirestore()

                    }



                }

    }




    private fun firebaseFirestore(){
        val nombre = view?.findViewById<EditText>(R.id.input_text_nombre)
        val email = view?.findViewById<EditText>(R.id.input_text_email)
        val telefono = view?.findViewById<EditText>(R.id.id_input_numero)

        val spinner = view?.findViewById<Spinner>(R.id.id_spinner)
        val textspinner = spinner!!.selectedItem.toString()

        //instancia de firestore
        val db = FirebaseFirestore.getInstance()

        val data = Account(
            nombre?.text.toString(),
            email?.text.toString(),
            telefono?.text.toString(),
            textspinner,
            urll.toString()
        )

        db.collection("accounts")
            .add(data)
            .addOnSuccessListener { documentReference ->
                Log.d(TAG, "DocumentSnapshot added with ID: ${documentReference.id}")

                /*idContainer!!.visibility = View.VISIBLE
                Handler().postDelayed({
                    idContainer!!.visibility = View.VISIBLE
                }, 3000L)*/

            }
            .addOnFailureListener { e ->
                Log.w(TAG, "Error adding document", e)
            }
    }

    private fun validarEnvio(): Boolean{
        val inputName = view?.findViewById<EditText>(R.id.input_text_nombre)
        val inputEmail = view?.findViewById<EditText>(R.id.input_text_email)

        if(inputName?.text.toString() == "") {
            Toast.makeText(activity, "Ingrese nombre", Toast.LENGTH_SHORT).show()
            input_text_nombre.findFocus()
            return false
        } else if(inputEmail?.text.toString() == "") {
            Toast.makeText(activity, "Ingrese email", Toast.LENGTH_SHORT).show()
            return false
        }else{
            return true
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed(uri: Uri) {
        listener?.onFragmentInteraction(uri)

    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }


    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
        fun succesMessage(idcontenedor: View)
        fun montarFragmentRecycler(frag: CollectionViewFragment)

    }

    companion object {

        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            BitlabFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}



