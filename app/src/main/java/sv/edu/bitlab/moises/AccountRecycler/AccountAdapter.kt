package sv.edu.bitlab.moises.AccountRecycler

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.firebase.ui.firestore.FirestoreRecyclerAdapter
import com.firebase.ui.firestore.FirestoreRecyclerOptions
import sv.edu.bitlab.moises.R
import sv.edu.bitlab.moises.dataclass.Account


class AccountAdapter(private val context: Context, options: FirestoreRecyclerOptions<Account>) : FirestoreRecyclerAdapter<Account, AccountAdapter.ViewHolder>(options){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.item_layout_account, parent, false)
        return ViewHolder(view)

    }

    override fun onBindViewHolder(holder: ViewHolder, p1: Int, p2: Account) {
        Glide.with(context).load(p2.accountImage)
            .placeholder(R.drawable.ic_loading)
            .error(R.drawable.ic_error)
            .fallback(R.drawable.ic_error)
            .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.ALL))
            .apply(RequestOptions().transform(CenterCrop(), RoundedCorners(150)))
            .into(holder.imageUser)
        holder.username.text = p2.accountName
        holder.email.text = p2.accountEmail
        holder.telefono.text = p2.accountPhone
        holder.meentere.text = p2.accountFoundOutBy

    }



    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            var imageUser = itemView.findViewById<ImageView>(R.id.user_imageview)
            var username = itemView.findViewById<TextView>(R.id.id_username)
            var email = itemView.findViewById<TextView>(R.id.id_user_email)
            var telefono = itemView.findViewById<TextView>(R.id.id_userphone)
            var meentere= itemView.findViewById<TextView>(R.id.id_me_entere)

    }


}