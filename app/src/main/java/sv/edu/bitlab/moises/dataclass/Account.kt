package sv.edu.bitlab.moises.dataclass

data class Account (var accountName: String?, var accountEmail: String?, var accountPhone: String?, var accountFoundOutBy: String?, var accountImage: String?)
{
    constructor():this("","","","","")
}